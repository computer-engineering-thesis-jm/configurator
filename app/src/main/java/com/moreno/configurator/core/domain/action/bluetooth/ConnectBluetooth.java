package com.moreno.configurator.core.domain.action.bluetooth;

import com.moreno.configurator.core.domain.model.TransportLayerService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.UserInformation;

import io.reactivex.Single;

public class ConnectBluetooth {

    public static final String BLUETOOTH_IDENTIFIER = "00001101-0000-1000-8000-00805f9b34fb";
    private final TransportLayerService transportLayerService;
    private final UserRepository userRepository;

    public ConnectBluetooth(TransportLayerService transportLayerService, UserRepository userRepository) {
        this.transportLayerService = transportLayerService;
        this.userRepository = userRepository;
    }

    public Single<Boolean> execute() {
        return Single.fromCallable(userRepository::getUserInformation)
                .map(UserInformation::getUuidToken)
                .flatMap(identifier -> transportLayerService.connect(BLUETOOTH_IDENTIFIER));
    }
}
