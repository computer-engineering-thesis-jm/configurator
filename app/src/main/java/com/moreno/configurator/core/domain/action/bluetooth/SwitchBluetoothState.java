package com.moreno.configurator.core.domain.action.bluetooth;

import com.moreno.configurator.core.domain.model.TransportLayerService;

import io.reactivex.Single;

public class SwitchBluetoothState {

    private final TransportLayerService transportLayerService;

    public SwitchBluetoothState(TransportLayerService transportLayerService) {
        this.transportLayerService = transportLayerService;
    }

    public Single<Boolean> execute() {
        return transportLayerService.switchState();
    }
}
