package com.moreno.configurator.core.domain.action.configure;

import io.reactivex.Observable;

public class ListenCompletedAck {

    private final Observable<String> bluetoothMessageObservable;

    public ListenCompletedAck(Observable<String> bluetoothMessageObservable) {

        this.bluetoothMessageObservable = bluetoothMessageObservable;
    }

    public Observable<Boolean> execute() {
        return bluetoothMessageObservable
                .map(response -> response.equals("c"));
    }
}
