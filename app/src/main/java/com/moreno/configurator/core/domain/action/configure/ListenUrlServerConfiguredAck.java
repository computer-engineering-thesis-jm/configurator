package com.moreno.configurator.core.domain.action.configure;

import io.reactivex.Observable;

public class ListenUrlServerConfiguredAck {

    private final Observable<String> bluetoothMessageObservable;

    public ListenUrlServerConfiguredAck(Observable<String> bluetoothMessageObservable) {

        this.bluetoothMessageObservable = bluetoothMessageObservable;
    }

    public Observable<Boolean> execute() {
        return bluetoothMessageObservable
                .map(response -> response.equals("u"));
    }
}
