package com.moreno.configurator.core.domain.action.configure;

import com.moreno.configurator.core.domain.model.TransportLayerService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.UserInformation;

import org.json.JSONObject;

import io.reactivex.Completable;

public class SendDeviceInformation {

    private final TransportLayerService transportLayerService;
    private final UserRepository repository;

    public SendDeviceInformation(TransportLayerService transportLayerService, UserRepository repository) {
        this.transportLayerService = transportLayerService;
        this.repository = repository;
    }

    public Completable execute() {
        return Completable.defer(() -> {
            UserInformation userInformation = repository.getUserInformation();
            String information = new JSONObject()
                    .put("did", userInformation.getDeviceId())
                    .put("tkn", userInformation.getUuidToken())
                    .toString();
            return transportLayerService.send(information);
        });
    }

}
