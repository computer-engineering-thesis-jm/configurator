package com.moreno.configurator.core.domain.action.configure;

import com.moreno.configurator.core.domain.model.TransportLayerService;

import org.json.JSONObject;

import io.reactivex.Completable;

public class SendUrlServerInformation {

    private final TransportLayerService transportLayerService;
    private final String apiEndpoint;
    private final Integer apiPort;

    public SendUrlServerInformation(TransportLayerService transportLayerService, String apiEndpoint, Integer apiPort) {
        this.transportLayerService = transportLayerService;
        this.apiEndpoint = apiEndpoint;
        this.apiPort = apiPort;
    }

    public Completable execute() {
        return Completable.defer(() -> {
            String information = new JSONObject()
                    .put("url", apiEndpoint)
                    .put("prt", apiPort)
                    .toString();
            return transportLayerService.send(information);
        });
    }
}
