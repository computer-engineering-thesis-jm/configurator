package com.moreno.configurator.core.domain.action.configure;

import com.moreno.configurator.core.domain.model.TransportLayerService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.NetworkInformation;

import org.json.JSONObject;

import io.reactivex.Completable;

public class SendWifiInformation {

    private final TransportLayerService transportLayerService;
    private final UserRepository repository;

    public SendWifiInformation(TransportLayerService transportLayerService, UserRepository repository) {
        this.transportLayerService = transportLayerService;
        this.repository = repository;
    }

    public Completable execute() {
        return Completable.defer(() -> {
            NetworkInformation networkInformation = repository.getNetworkInformation();
            String information = new JSONObject()
                    .put("sid", networkInformation.getSsid())
                    .put("pss", networkInformation.getPassword())
                    .toString();
            return transportLayerService.send(information);
        });
    }
}
