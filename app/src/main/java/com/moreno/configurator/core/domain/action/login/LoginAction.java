package com.moreno.configurator.core.domain.action.login;

import com.moreno.configurator.core.domain.model.ApiService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.LoginResult;
import com.moreno.configurator.core.domain.model.network.UserInformation;

import io.reactivex.Single;

public class LoginAction {

    private static final String EMPTY = "";

    private final ApiService apiService;
    private final UserRepository userRepository;

    public LoginAction(ApiService apiService, UserRepository userRepository) {
        this.apiService = apiService;
        this.userRepository = userRepository;
    }

    public Single<Boolean> execute(String deviceId, String password) {
        return Single.defer(() -> {
            if (EMPTY.equals(deviceId) || EMPTY.equals(password)) {
                return Single.just(false);
            }

            return apiService.login(deviceId, password)
                    .doOnSuccess(loginResult -> {
                        String uuidToken = loginResult.getUuidToken();
                        userRepository.putUserInformation(new UserInformation(deviceId, password, uuidToken));
                    })
                    .map(LoginResult::getSuccess);
        });
    }
}
