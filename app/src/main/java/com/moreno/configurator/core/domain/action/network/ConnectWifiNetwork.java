package com.moreno.configurator.core.domain.action.network;

import com.moreno.configurator.core.domain.model.NetworkService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.NetworkInformation;

import io.reactivex.Completable;
import io.reactivex.Single;

public class ConnectWifiNetwork {

    private final NetworkService networkService;
    private final UserRepository userRepository;

    public ConnectWifiNetwork(NetworkService networkService, UserRepository userRepository) {
        this.networkService = networkService;
        this.userRepository = userRepository;
    }

    public Completable execute(String ssid, String password) {
        return networkService.isValidNetwork(ssid, password)
                .filter(isValid -> isValid)
                .doOnSuccess(bool -> userRepository.putNetwork(new NetworkInformation(ssid, password)))
                .switchIfEmpty(createInvalidNetworkException(ssid, password))
                .toCompletable();
    }

    private Single<Boolean> createInvalidNetworkException(String ssid, String password) {
        return Single.error(new InvalidNetworkException("Cannot connect to network=" + ssid +
                " with password=" + password));
    }

    public class InvalidNetworkException extends RuntimeException {
        InvalidNetworkException(String message) {
            super(message);
        }
    }
}
