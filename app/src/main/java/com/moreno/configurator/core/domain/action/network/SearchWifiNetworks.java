package com.moreno.configurator.core.domain.action.network;

import io.reactivex.Observable;

public class SearchWifiNetworks {

    private final Observable<String> ssidNetworksObservable;

    public SearchWifiNetworks(Observable<String> ssidNetworksObservable) {
        this.ssidNetworksObservable = ssidNetworksObservable;
    }

    public Observable<String> execute() {
        return ssidNetworksObservable;
    }
}
