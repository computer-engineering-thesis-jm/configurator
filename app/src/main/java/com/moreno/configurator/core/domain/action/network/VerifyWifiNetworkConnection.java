package com.moreno.configurator.core.domain.action.network;


import com.moreno.configurator.core.domain.model.NetworkService;

import io.reactivex.Single;

public class VerifyWifiNetworkConnection {

    private final NetworkService networkService;

    public VerifyWifiNetworkConnection(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Single<Boolean> execute() {
        return networkService.isActive();
    }
}
