package com.moreno.configurator.core.domain.model;

import com.moreno.configurator.core.domain.model.network.LoginResult;

import io.reactivex.Single;

public interface ApiService {

    Single<LoginResult> login(String deviceId, String password);
}
