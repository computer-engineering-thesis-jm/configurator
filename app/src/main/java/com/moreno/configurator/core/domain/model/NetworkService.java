package com.moreno.configurator.core.domain.model;

import io.reactivex.Single;

public interface NetworkService {

    Single<Boolean> isActive();

    Single<Boolean> isValidNetwork(String ssid, String password);
}
