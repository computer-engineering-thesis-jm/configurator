package com.moreno.configurator.core.domain.model;


public interface TimeService {
    long getTime();
}
