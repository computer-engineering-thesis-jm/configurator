package com.moreno.configurator.core.domain.model;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface TransportLayerService {

    Single<Boolean> switchState();

    Single<Boolean> connect(String identifier);

    Completable send(String information);
}
