package com.moreno.configurator.core.domain.model;

import com.moreno.configurator.core.domain.model.network.NetworkInformation;
import com.moreno.configurator.core.domain.model.network.UserInformation;


public interface UserRepository {
    void putUserInformation(UserInformation userInformation);

    UserInformation getUserInformation();

    void putNetwork(NetworkInformation networkInformation);

    NetworkInformation getNetworkInformation();
}
