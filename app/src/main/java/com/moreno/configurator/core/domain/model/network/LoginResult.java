package com.moreno.configurator.core.domain.model.network;


import java.util.Objects;

public class LoginResult {

    private final String deviceId;
    private final String password;
    private final Boolean success;
    private final String uuidToken;

    public LoginResult(String deviceId, String password, Boolean success, String uuidToken) {
        this.deviceId = deviceId;
        this.password = password;
        this.success = success;
        this.uuidToken = uuidToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getUuidToken() {
        return uuidToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginResult that = (LoginResult) o;
        return Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(password, that.password) &&
                Objects.equals(uuidToken, that.uuidToken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, password, uuidToken);
    }

    public static LoginResult failure(String deviceId, String password) {
        return new LoginResult(deviceId, password, false, "");
    }

    public static LoginResult success(String deviceId, String password, String token) {
        return new LoginResult(deviceId, password, true, token);
    }
}
