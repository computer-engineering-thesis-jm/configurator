package com.moreno.configurator.core.domain.model.network;

public class NetworkInformation {

    private final String ssid;
    private final String password;

    public NetworkInformation(String ssid, String password) {
        this.ssid = ssid;
        this.password = password;
    }

    public String getSsid() {
        return ssid;
    }

    public String getPassword() {
        return password;
    }
}
