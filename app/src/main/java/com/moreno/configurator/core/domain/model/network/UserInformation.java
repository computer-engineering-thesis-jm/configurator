package com.moreno.configurator.core.domain.model.network;

public class UserInformation {

    private final String deviceId;
    private final String password;
    private final String uuidToken;

    public UserInformation(String deviceId, String password, String uuidToken) {
        this.deviceId = deviceId;
        this.password = password;
        this.uuidToken = uuidToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getPassword() {
        return password;
    }

    public String getUuidToken() {
        return uuidToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInformation that = (UserInformation) o;
        return getDeviceId().equals(that.getDeviceId()) &&
                getPassword().equals(that.getPassword()) &&
                getUuidToken().equals(that.getUuidToken());
    }

    @Override
    public int hashCode() {
        return getDeviceId().hashCode() + getPassword().hashCode() + getUuidToken().hashCode();
    }
}
