package com.moreno.configurator.core.infrastructure;

import com.moreno.configurator.core.domain.model.TimeService;

public class ActualTimeService implements TimeService {

    @Override
    public long getTime() {
        return System.currentTimeMillis();
    }
}
