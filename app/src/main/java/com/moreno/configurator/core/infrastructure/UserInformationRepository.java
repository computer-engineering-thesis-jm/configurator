package com.moreno.configurator.core.infrastructure;

import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.NetworkInformation;
import com.moreno.configurator.core.domain.model.network.UserInformation;


public class UserInformationRepository implements UserRepository {

    private UserInformation userInformation;
    private NetworkInformation networkInformation;

    @Override
    public void putUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    @Override
    public UserInformation getUserInformation() {
        return this.userInformation;
    }

    @Override
    public void putNetwork(NetworkInformation networkInformation) {
        this.networkInformation = networkInformation;
    }

    @Override
    public NetworkInformation getNetworkInformation() {
        return this.networkInformation;
    }
}
