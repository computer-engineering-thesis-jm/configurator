package com.moreno.configurator.core.infrastructure;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.moreno.configurator.core.domain.model.ApiService;
import com.moreno.configurator.core.domain.model.network.LoginResult;

import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserInformationService implements ApiService {

    private static final MediaType CONTENT_TYPE = MediaType.parse("application/json");
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String USER_LOGIN_PATH = "/user/login";
    private static final String TOKEN_KEY = "token";
    private static final String SUCCESS_KEY = "success";
    private static final String CONTENT_KEY = "content";
    private static final String COLON_SEPARATOR = ":";

    private final String endpoint;
    private final Integer port;
    private final String authorizationToken;
    private final OkHttpClient client;

    public UserInformationService(OkHttpClient client, String endpoint, Integer port, String authorizationToken) {
        this.endpoint = endpoint;
        this.port = port;
        this.authorizationToken = authorizationToken;
        this.client = client;
    }

    @Override
    public Single<LoginResult> login(String deviceId, String password) {
        return Single.create(emitter -> {
            Request request = new Request.Builder()
                    .header(AUTHORIZATION_HEADER, authorizationToken)
                    .post(RequestBody.create(CONTENT_TYPE, toJson(deviceId, password)))
                    .url(makeUrl())
                    .build();

            try {
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        emitter.onSuccess(LoginResult.failure(deviceId, password));
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (response.isSuccessful()) {
                            String bodyAsString = response.body().string();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonObject = jsonParser.parse(bodyAsString).getAsJsonObject();
                            if (jsonObject.get(SUCCESS_KEY).getAsBoolean()) {
                                String token = getToken(jsonParser, jsonObject);
                                emitter.onSuccess(LoginResult.success(deviceId, password, token));

                            } else {
                                failureResponse(deviceId, password, emitter);
                            }
                        } else {
                            failureResponse(deviceId, password, emitter);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                failureResponse(deviceId, password, emitter);
            }
        });
    }

    private String getToken(JsonParser jsonParser, JsonObject jsonObject) {
        String contentAsString = jsonObject.get(CONTENT_KEY).toString()
                .replaceAll("^\"|\"$", "")
                .replaceAll("\\\\", "");
        JsonObject content = jsonParser.parse(contentAsString).getAsJsonObject();
        return content.get(TOKEN_KEY).toString().replaceAll("^\"|\"$", "");
    }

    @NonNull
    private String makeUrl() {
        return endpoint + COLON_SEPARATOR + port + USER_LOGIN_PATH;
    }

    private void failureResponse(String deviceId, String password, SingleEmitter<LoginResult> emitter) {
        emitter.onSuccess(LoginResult.failure(deviceId, password));
    }

    private String toJson(String deviceId, String password) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("deviceId", deviceId);
        jsonObject.addProperty("password", password);
        return jsonObject.toString();
    }
}
