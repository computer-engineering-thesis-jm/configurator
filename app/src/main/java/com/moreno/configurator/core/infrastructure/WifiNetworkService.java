package com.moreno.configurator.core.infrastructure;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import com.moreno.configurator.core.domain.model.NetworkService;

import io.reactivex.Single;

public class WifiNetworkService implements NetworkService {

    private final WifiManager wifiManager;

    public WifiNetworkService(WifiManager wifiManager) {
        this.wifiManager = wifiManager;
    }

    @Override
    public Single<Boolean> isActive() {
        return Single.fromCallable(wifiManager::isWifiEnabled);
    }

    @Override
    public Single<Boolean> isValidNetwork(String ssid, String password) {
        return Single.fromCallable(() -> getWifiConfiguration(ssid, password))
                .map(this::canConnect);
    }

    private WifiConfiguration getWifiConfiguration(String ssid, String password) {
        WifiConfiguration config = new WifiConfiguration();
        config.SSID = "\"" + ssid + "\"";
        config.preSharedKey = "\"" + password + "\"";
        return config;
    }

    private Boolean canConnect(WifiConfiguration config) {
        int netId = wifiManager.addNetwork(config);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        return wifiManager.reconnect();
    }
}
