package com.moreno.configurator.core.infrastructure.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.moreno.configurator.core.domain.model.TransportLayerService;

import java.util.Arrays;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class BluetoothTransportService implements TransportLayerService {

    private final BluetoothService bluetoothService;

    public BluetoothTransportService(BluetoothService bluetoothService) {
        this.bluetoothService = bluetoothService;
    }

    @Override
    public Single<Boolean> switchState() {
        return Single.fromCallable(bluetoothService::isEnabled)
                .filter(isEnabled -> isEnabled)
                .map(bool -> bluetoothService.enable())
                .toSingle(bluetoothService.disable());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    // FIXME: Connect bluetooth without the identifier because Arduino is hard to configure his uuid connection.
    // Bluetooth module H-05 = 00001101-0000-1000-8000-00805f9b34fb
    public Single<Boolean> connect(String identifier) {
        return Single.fromCallable(bluetoothService::getBoundedDevices)
                .flatMap(bluetoothDevices -> getBluetoothDeviceByIdentifier(identifier, bluetoothDevices))
                .doOnSuccess(bluetoothDevice -> bluetoothService.connect(bluetoothDevice, identifier))
                .map(bluetoothDevice -> bluetoothService.isEnabled())
                .onErrorResumeNext(Single.fromCallable(() -> false));
    }

    private Single<BluetoothDevice> getBluetoothDeviceByIdentifier(String identifier, Set<BluetoothDevice> bluetoothDevices) {
        return Observable.fromIterable(bluetoothDevices)
                .flatMap(bluetoothDevice -> selectBluetoothDeviceThroughIdentifier(identifier, bluetoothDevice))
                .firstOrError();
    }

    private Observable<BluetoothDevice> selectBluetoothDeviceThroughIdentifier(String identifier, BluetoothDevice bluetoothDevice) {
        return Observable.fromIterable(Arrays.asList(bluetoothDevice.getUuids()))
                .map(parcelUuid -> parcelUuid.getUuid().toString())
                .filter(uuid -> uuid.equals(identifier))
                .map(parcelUuid -> bluetoothDevice);
    }

    @Override
    public Completable send(String information) {
        return Completable.fromAction(() -> bluetoothService.write(information.getBytes()));
    }
}
