package com.moreno.configurator.core.util;

import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;

import com.moreno.configurator.core.domain.action.bluetooth.ConnectBluetooth;
import com.moreno.configurator.core.domain.action.bluetooth.SwitchBluetoothState;
import com.moreno.configurator.core.domain.action.configure.ListenCompletedAck;
import com.moreno.configurator.core.domain.action.configure.ListenDeviceConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.ListenUrlServerConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.ListenWifiConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.SendDeviceInformation;
import com.moreno.configurator.core.domain.action.configure.SendUrlServerInformation;
import com.moreno.configurator.core.domain.action.configure.SendWifiInformation;
import com.moreno.configurator.core.domain.action.login.LoginAction;
import com.moreno.configurator.core.domain.action.network.ConnectWifiNetwork;
import com.moreno.configurator.core.domain.action.network.SearchWifiNetworks;
import com.moreno.configurator.core.domain.action.network.VerifyWifiNetworkConnection;
import com.moreno.configurator.core.domain.model.ApiService;
import com.moreno.configurator.core.domain.model.NetworkService;
import com.moreno.configurator.core.domain.model.TimeService;
import com.moreno.configurator.core.domain.model.TransportLayerService;
import com.moreno.configurator.core.infrastructure.ActualTimeService;
import com.moreno.configurator.core.infrastructure.UserInformationRepository;
import com.moreno.configurator.core.infrastructure.UserInformationService;
import com.moreno.configurator.core.infrastructure.WifiNetworkService;
import com.moreno.configurator.core.infrastructure.bluetooth.BluetoothService;
import com.moreno.configurator.core.infrastructure.bluetooth.BluetoothTransportService;
import com.moreno.configurator.presentation.action.FillFieldsWithInformation;
import com.moreno.configurator.presentation.presenter.ChooseNetworkPresenter;
import com.moreno.configurator.presentation.presenter.ConnectBluetoothPresenter;
import com.moreno.configurator.presentation.presenter.ConnectNetworkPresenter;
import com.moreno.configurator.presentation.presenter.FinishConfigurationPresenter;
import com.moreno.configurator.presentation.presenter.LoginPresenter;
import com.moreno.configurator.presentation.view.ActivityNavigator;
import com.moreno.configurator.presentation.view.fragment.ChooseNetworkFragment;
import com.moreno.configurator.presentation.view.fragment.ConnectBluetoothFragment;
import com.moreno.configurator.presentation.view.fragment.ConnectNetworkFragment;
import com.moreno.configurator.presentation.view.fragment.FinishConfigurationFragment;
import com.moreno.configurator.presentation.view.fragment.LoginFragment;

import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import okhttp3.OkHttpClient;

public class Provider {

    private static final String API_ENDPOINT = "http://192.168.1.10";
    private static final Integer API_PORT = 8080;
    private static final String AUTHORIZATION_TOKEN = "secret";
    private static final UUID uuidTokenToReceive = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static UserInformationRepository userInformationRepository;
    private static ActivityNavigator activityNavigator;
    private static PublishSubject<String> ssidPublishSubject;
    private static TimeService timeService;
    private static SendUrlServerInformation sendUrlServerInformation;
    private static FillFieldsWithInformation fillFieldsWithInformation;
    private static ApiService apiService;
    private static TransportLayerService bluetoothTransportService;
    private static BluetoothService bluetoothService;
    private static PublishSubject<String> bluetoothReaderPublishSubject;
    private static ListenCompletedAck listenCompletedAck;
    private static SendDeviceInformation sendDeviceInformation;
    private static SendWifiInformation sendWifiInformation;
    private static ListenDeviceConfiguredAck listenDeviceConfiguredAck;
    private static ListenWifiConfiguredAck listenWifiConfiguredAck;
    private static ListenUrlServerConfiguredAck listenUrlServerConfiguredAck;

    public static LoginPresenter getLoginPresenter(LoginFragment view) {
        return new LoginPresenter(view, getLoginAction(), getNavigator());
    }

    @NonNull
    private static ActivityNavigator getNavigator() {
        if (activityNavigator == null) {
            activityNavigator = new ActivityNavigator();
        }
        return activityNavigator;
    }

    private static LoginAction getLoginAction() {
        return new LoginAction(getApiService(), getUserRepository());
    }

    @NonNull
    private static UserInformationRepository getUserRepository() {
        if (userInformationRepository == null) {
            userInformationRepository = new UserInformationRepository();
        }
        return userInformationRepository;
    }

    @NonNull
    private static ApiService getApiService() {
        if (apiService == null) {
            apiService = new UserInformationService(getOkHttpClient(), API_ENDPOINT, API_PORT, AUTHORIZATION_TOKEN);
        }
        return apiService;
    }

    private static OkHttpClient getOkHttpClient() {
        return new OkHttpClient();
    }

    public static ConnectBluetoothPresenter getConnectBluetoothPresenter(ConnectBluetoothFragment view) {
        return new ConnectBluetoothPresenter(view, getSwitchBluetoothStateAction(), getConnectBluetooth(), getNavigator());
    }

    private static ConnectBluetooth getConnectBluetooth() {
        return new ConnectBluetooth(getTransportLayerService(), getUserRepository());
    }

    private static SwitchBluetoothState getSwitchBluetoothStateAction() {
        return new SwitchBluetoothState(getTransportLayerService());
    }

    private static TransportLayerService getTransportLayerService() {
        if (bluetoothTransportService == null) {
            bluetoothTransportService = new BluetoothTransportService(getBluetoothService());
        }
        return bluetoothTransportService;
    }

    private static BluetoothService getBluetoothService() {
        if (bluetoothService == null) {
            bluetoothService = new BluetoothService(uuidTokenToReceive, getBluetoothMessagePublishSubject());
        }
        return bluetoothService;
    }

    private static PublishSubject<String> getBluetoothMessagePublishSubject() {
        if (bluetoothReaderPublishSubject == null) {
            bluetoothReaderPublishSubject = PublishSubject.create();
        }
        return bluetoothReaderPublishSubject;
    }

    public static ConnectNetworkPresenter getConnectNetworkPresenter(ConnectNetworkFragment view, WifiManager wifiManager) {
        return new ConnectNetworkPresenter(view, getConnectWifiNetwork(wifiManager), getNavigator());
    }

    private static ConnectWifiNetwork getConnectWifiNetwork(WifiManager wifiManager) {
        return new ConnectWifiNetwork(getNetworkService(wifiManager), getUserRepository());
    }

    public static ChooseNetworkPresenter getChooseNetworkPresenter(ChooseNetworkFragment view, WifiManager wifiManager) {
        return new ChooseNetworkPresenter(view, getVerifyWifiNetworkConnection(wifiManager),
                getSearchWifiNetworks(), getSsidPublishSubject(), getNavigator());
    }

    private static PublishSubject<String> getSsidPublishSubject() {
        if (ssidPublishSubject == null) {
            ssidPublishSubject = PublishSubject.create();
        }
        return ssidPublishSubject;
    }

    private static VerifyWifiNetworkConnection getVerifyWifiNetworkConnection(WifiManager wifiManager) {
        return new VerifyWifiNetworkConnection(getNetworkService(wifiManager));
    }

    private static SearchWifiNetworks getSearchWifiNetworks() {
        return new SearchWifiNetworks(getSsidObservable());
    }

    private static Observable<String> getSsidObservable() {
        if (ssidPublishSubject == null) {
            ssidPublishSubject = PublishSubject.create();
        }
        return ssidPublishSubject;
    }

    private static NetworkService getNetworkService(WifiManager wifiManager) {
        return new WifiNetworkService(wifiManager);
    }

    public static FinishConfigurationPresenter getFinishConfigurationPresenter(FinishConfigurationFragment view) {
        return new FinishConfigurationPresenter(view,
                getFillFieldsWithInformation(view),
                getSendDeviceInformation(),
                getSendWifiInformation(),
                getSendUrlServerInformation(),
                getListenCompletedAck(),
                getListenDeviceConfiguredAck(),
                getListenWifiConfiguredAck(),
                getListenUrlServerConfiguredAck(),
                getNavigator());
    }

    private static ListenCompletedAck getListenCompletedAck() {
        if (listenCompletedAck == null) {
            listenCompletedAck = new ListenCompletedAck(getBluetoothMessageObservable());
        }
        return listenCompletedAck;
    }

    private static ListenDeviceConfiguredAck getListenDeviceConfiguredAck() {
        if (listenDeviceConfiguredAck == null) {
            listenDeviceConfiguredAck = new ListenDeviceConfiguredAck(getBluetoothMessageObservable());
        }
        return listenDeviceConfiguredAck;
    }

    private static ListenWifiConfiguredAck getListenWifiConfiguredAck() {
        if (listenWifiConfiguredAck == null) {
            listenWifiConfiguredAck = new ListenWifiConfiguredAck(getBluetoothMessageObservable());
        }
        return listenWifiConfiguredAck;
    }

    private static ListenUrlServerConfiguredAck getListenUrlServerConfiguredAck() {
        if (listenUrlServerConfiguredAck == null) {
            listenUrlServerConfiguredAck = new ListenUrlServerConfiguredAck(getBluetoothMessageObservable());
        }
        return listenUrlServerConfiguredAck;
    }

    private static Observable<String> getBluetoothMessageObservable() {
        return getBluetoothMessagePublishSubject();
    }

    private static SendDeviceInformation getSendDeviceInformation() {
        if (sendDeviceInformation == null) {
            sendDeviceInformation = new SendDeviceInformation(getTransportLayerService(), getUserRepository());
        }
        return sendDeviceInformation;
    }

    private static SendWifiInformation getSendWifiInformation() {
        if (sendWifiInformation == null) {
            sendWifiInformation = new SendWifiInformation(getTransportLayerService(), getUserRepository());
        }
        return sendWifiInformation;
    }

    private static SendUrlServerInformation getSendUrlServerInformation() {
        if (sendUrlServerInformation == null) {
            sendUrlServerInformation = new SendUrlServerInformation(getTransportLayerService(), API_ENDPOINT, API_PORT);
        }
        return sendUrlServerInformation;
    }

    private static TimeService getTimeService() {
        if (timeService == null) {
            timeService = new ActualTimeService();
        }
        return timeService;
    }

    private static FillFieldsWithInformation getFillFieldsWithInformation(FinishConfigurationFragment view) {
        if (fillFieldsWithInformation == null) {
            fillFieldsWithInformation = new FillFieldsWithInformation(view, getTimeService(), getUserRepository());
        }
        return fillFieldsWithInformation;
    }


}
