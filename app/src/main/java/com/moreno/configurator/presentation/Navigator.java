package com.moreno.configurator.presentation;

import android.content.Context;
import android.support.v4.app.Fragment;

public interface Navigator {
    void navigateToConnectBluetoothActivity(Fragment fragment);

    void navigateToChooseNetworkActivity(Fragment fragment);

    void navigateToConnectNetworkActivity(Fragment fragment, String ssid);

    void navigateToFinishConfigurationActivity(Fragment fragment);

    void showToast(Context context, String message);
}
