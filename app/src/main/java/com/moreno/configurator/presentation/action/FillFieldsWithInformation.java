package com.moreno.configurator.presentation.action;

import com.moreno.configurator.core.domain.model.TimeService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.NetworkInformation;
import com.moreno.configurator.core.domain.model.network.UserInformation;
import com.moreno.configurator.presentation.view.fragment.FinishConfigurationFragment;

import java.util.Date;

import io.reactivex.Completable;

public class FillFieldsWithInformation {

    private final FinishConfigurationFragment view;
    private final TimeService timeService;
    private final UserRepository repository;

    public FillFieldsWithInformation(FinishConfigurationFragment view, TimeService timeService,
                                     UserRepository repository) {
        this.view = view;
        this.timeService = timeService;
        this.repository = repository;
    }

    public Completable execute() {
        return Completable.fromAction(() -> {
            UserInformation userInformation = repository.getUserInformation();
            view.setDeviceId(userInformation.getDeviceId());

            NetworkInformation networkInformation = repository.getNetworkInformation();
            view.setNetworkSsid(networkInformation.getSsid());

            long time = timeService.getTime();
            view.setTime(new Date(time).toString());
        });
    }
}
