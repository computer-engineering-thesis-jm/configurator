package com.moreno.configurator.presentation.presenter;


import android.widget.AdapterView;

import com.moreno.configurator.core.domain.action.network.SearchWifiNetworks;
import com.moreno.configurator.core.domain.action.network.VerifyWifiNetworkConnection;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.view.fragment.ChooseNetworkFragment;
import com.moreno.configurator.presentation.view.model.NetworkView;

import io.reactivex.subjects.PublishSubject;

public class ChooseNetworkPresenter {

    private final ChooseNetworkFragment view;
    private final VerifyWifiNetworkConnection verifyWifiNetworkConnection;
    private final SearchWifiNetworks searchNetworks;
    private final Navigator navigator;

    public ChooseNetworkPresenter(ChooseNetworkFragment chooseNetworkFragment,
                                  VerifyWifiNetworkConnection verifyWifiNetworkConnection,
                                  SearchWifiNetworks searchNetworks,
                                  PublishSubject<String> ssidNetworksSubject, Navigator navigator) {
        this.view = chooseNetworkFragment;
        this.verifyWifiNetworkConnection = verifyWifiNetworkConnection;
        this.searchNetworks = searchNetworks;
        this.navigator = navigator;
        this.view.registerWifiReceiver(ssidNetworksSubject);
    }

    public void onResume() {
        verifyWifiNetworkConnection.execute()
                .filter(isConnected -> isConnected)
                .flatMapObservable(bool -> searchNetworks.execute())
                .subscribe(this::addNetworkView);
        view.startScan();
    }

    private void addNetworkView(String networkSsid) {
        view.addNetworkView(networkSsid);
    }

    public AdapterView.OnItemClickListener onClickNetworkItemListener() {
        return (parent, view1, position, id) -> {
            NetworkView selectedItem = (NetworkView) parent.getItemAtPosition(position);
            navigator.navigateToConnectNetworkActivity(view, selectedItem.getSsid());
        };
    }
}
