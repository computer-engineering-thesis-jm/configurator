package com.moreno.configurator.presentation.presenter;

import com.moreno.configurator.core.domain.action.bluetooth.ConnectBluetooth;
import com.moreno.configurator.core.domain.action.bluetooth.SwitchBluetoothState;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.view.fragment.ConnectBluetoothFragment;

public class ConnectBluetoothPresenter {

    private final ConnectBluetoothFragment view;
    private final SwitchBluetoothState switchBluetoothState;
    private final ConnectBluetooth connectBluetooth;
    private final Navigator navigator;

    public ConnectBluetoothPresenter(ConnectBluetoothFragment connectBluetoothFragment, SwitchBluetoothState switchBluetoothState,
                                     ConnectBluetooth connectBluetooth, Navigator navigator) {
        this.view = connectBluetoothFragment;
        this.switchBluetoothState = switchBluetoothState;
        this.connectBluetooth = connectBluetooth;
        this.navigator = navigator;
    }

    public void onBluetoothToggleIsPressed() {
        switchBluetoothState.execute()
                .subscribe(view::setBluetoothToggleState);
    }

    public void onResume() {
        onBluetoothToggleIsPressed();
    }

    public void connect() {
        connectBluetooth.execute()
                .doOnSuccess(this::showToastOnInvalidConnection)
                .filter(isValid -> isValid)
                .subscribe(wasValid -> navigator.navigateToFinishConfigurationActivity(view),
                        throwable -> {
                            throwable.printStackTrace();
                            navigator.showToast(view.getContext(), "WTF " + throwable.getMessage());
                        });
    }

    private void showToastOnInvalidConnection(Boolean isValid) {
        if (!isValid) {
            navigator.showToast(view.getContext(), "Cannot connect bluetooth");
        }
    }
}
