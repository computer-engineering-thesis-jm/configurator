package com.moreno.configurator.presentation.presenter;

import android.content.Intent;

import com.moreno.configurator.core.domain.action.network.ConnectWifiNetwork;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.view.fragment.ConnectNetworkFragment;

public class ConnectNetworkPresenter {

    private final ConnectNetworkFragment view;
    private final ConnectWifiNetwork connectWifiNetwork;
    private final Navigator navigator;

    public ConnectNetworkPresenter(ConnectNetworkFragment view,
                                   ConnectWifiNetwork connectWifiNetwork, Navigator navigator) {
        this.view = view;
        this.connectWifiNetwork = connectWifiNetwork;
        this.navigator = navigator;
    }

    public void onResume() {
        view.setSsid(getSsid());
    }

    private String getSsid() {
        Intent intent = view.getIntent();
        return intent.getStringExtra("NETWORK_SSID");
    }

    public void connect() {
        connectWifiNetwork.execute(view.getSsid(), view.getPassword())
                .subscribe(() -> navigator.navigateToConnectBluetoothActivity(view));
    }
}
