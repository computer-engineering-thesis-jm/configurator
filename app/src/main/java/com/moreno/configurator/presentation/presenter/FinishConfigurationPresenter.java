package com.moreno.configurator.presentation.presenter;

import android.support.v4.app.FragmentActivity;

import com.moreno.configurator.core.domain.action.configure.ListenCompletedAck;
import com.moreno.configurator.core.domain.action.configure.ListenDeviceConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.ListenUrlServerConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.ListenWifiConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.SendDeviceInformation;
import com.moreno.configurator.core.domain.action.configure.SendUrlServerInformation;
import com.moreno.configurator.core.domain.action.configure.SendWifiInformation;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.action.FillFieldsWithInformation;
import com.moreno.configurator.presentation.view.fragment.FinishConfigurationFragment;

import java.util.concurrent.TimeUnit;

public class FinishConfigurationPresenter {

    private final FinishConfigurationFragment view;
    private final SendDeviceInformation sendDeviceInformation;
    private final SendWifiInformation sendWifiInformation;
    private final SendUrlServerInformation sendUrlServerInformation;
    private final FillFieldsWithInformation fillFieldsWithInformation;
    private final ListenCompletedAck listenCompletedAck;
    private final ListenDeviceConfiguredAck listenDeviceConfiguredAck;
    private final ListenWifiConfiguredAck listenWifiConfiguredAck;
    private final ListenUrlServerConfiguredAck listenUrlServerConfiguredAck;
    private final Navigator navigator;

    public FinishConfigurationPresenter(FinishConfigurationFragment view,
                                        FillFieldsWithInformation fillFieldsWithInformation,
                                        SendDeviceInformation sendDeviceInformation,
                                        SendWifiInformation sendWifiInformation,
                                        SendUrlServerInformation sendUrlServerInformation,
                                        ListenCompletedAck listenCompletedAck,
                                        ListenDeviceConfiguredAck listenDeviceConfiguredAck,
                                        ListenWifiConfiguredAck listenWifiConfiguredAck,
                                        ListenUrlServerConfiguredAck listenUrlServerConfiguredAck,
                                        Navigator navigator) {
        this.view = view;
        this.sendDeviceInformation = sendDeviceInformation;
        this.sendWifiInformation = sendWifiInformation;
        this.sendUrlServerInformation = sendUrlServerInformation;
        this.fillFieldsWithInformation = fillFieldsWithInformation;
        this.listenCompletedAck = listenCompletedAck;
        this.listenDeviceConfiguredAck = listenDeviceConfiguredAck;
        this.listenWifiConfiguredAck = listenWifiConfiguredAck;
        this.listenUrlServerConfiguredAck = listenUrlServerConfiguredAck;
        this.navigator = navigator;
    }

    public void onCreate() {
        view.enableDeviceButton();
        view.disableExitButton();
        view.disableWifiButton();
        view.disableUrlButton();
    }

    public void onResume() {

        listenCompletedAck.execute()
                .filter(configured -> configured)
                .subscribe(response -> {
                    view.enableExitButton();
                    showToastOnMainThread("COMPLETED!");
                });

        listenDeviceConfiguredAck.execute()
                .filter(configured -> configured)
                .subscribe(configured -> {
                    view.disableDeviceButton();
                    view.enableWifiButton();
                    showToastOnMainThread("DEVICE CONFIGURED!");
                });

        listenWifiConfiguredAck.execute()
                .filter(configured -> configured)
                .subscribe(configured -> {
                    view.disableWifiButton();
                    view.enableUrlButton();
                    showToastOnMainThread("WIFI CONFIGURED!");
                });

        listenUrlServerConfiguredAck.execute()
                .filter(configured -> configured)
                .subscribe(configured -> {
                    view.disableUrlButton();
                    showToastOnMainThread("URL CONFIGURED!");
                });

        fillFieldsWithInformation.execute()
                .subscribe();
    }

    public void sendDeviceConfiguration() {
        sendDeviceInformation.execute()
                .subscribe();
    }

    public void sendWifiConfiguration() {
        sendWifiInformation.execute()
                .subscribe();
    }

    public void sendUrlServerConfiguration() {
        sendUrlServerInformation.execute()
                .subscribe();
    }

    private void showToastOnMainThread(String message) {
        if (view.getActivity() == null) return;
        view.getActivity().runOnUiThread(() -> navigator.showToast(view.getContext(), message));
    }

    public void exit() {
        FragmentActivity activity = view.getActivity();
        if (activity != null) activity.finishAffinity();
        System.exit(0);
    }
}
