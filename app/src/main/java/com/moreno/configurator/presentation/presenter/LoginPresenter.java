package com.moreno.configurator.presentation.presenter;

import android.text.Editable;
import android.text.TextWatcher;

import com.moreno.configurator.core.domain.action.login.LoginAction;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.view.fragment.LoginFragment;

import io.reactivex.Maybe;
import io.reactivex.Single;

public class LoginPresenter {

    private final LoginFragment view;
    private final LoginAction loginAction;
    private final Navigator navigator;

    public LoginPresenter(LoginFragment view, LoginAction loginAction, Navigator navigator) {
        this.view = view;
        this.loginAction = loginAction;
        this.navigator = navigator;
    }

    public void onResume() {
        view.disableSignInButton();
        setTextChangedListenerOnDeviceIdEditText();
    }

    public Maybe<Boolean> signIn() {
        return Single.defer(() -> {
            String deviceId = view.getDeviceId();
            String password = view.getPassword();
            return loginAction.execute(deviceId, password);
        }).filter(isSuccess -> isSuccess)
                .doOnSuccess(isSuccess -> navigator.navigateToChooseNetworkActivity(view));
    }

    public void onDestroy() {

    }

    private void setTextChangedListenerOnDeviceIdEditText() {
        view.setTextChangedListenerOnDeviceIdEditText(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    view.enableSignInButton();
                } else {
                    view.disableSignInButton();
                }
            }
        });
    }
}
