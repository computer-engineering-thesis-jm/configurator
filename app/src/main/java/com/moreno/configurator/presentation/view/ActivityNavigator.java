package com.moreno.configurator.presentation.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.widget.Toast;

import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.view.activity.ChooseNetworkActivity;
import com.moreno.configurator.presentation.view.activity.ConnectBluetoothActivity;
import com.moreno.configurator.presentation.view.activity.ConnectNetworkActivity;
import com.moreno.configurator.presentation.view.activity.FinishConfigurationActivity;

public class ActivityNavigator implements Navigator {

    @Override
    public void navigateToConnectBluetoothActivity(Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), ConnectBluetoothActivity.class);
        fragment.startActivity(intent);
    }

    @Override
    public void navigateToChooseNetworkActivity(Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), ChooseNetworkActivity.class);
        fragment.startActivity(intent);
    }

    @Override
    public void navigateToConnectNetworkActivity(Fragment fragment, String ssid) {
        Intent intent = new Intent(fragment.getActivity(), ConnectNetworkActivity.class);
        intent.putExtra("NETWORK_SSID", ssid);
        fragment.startActivity(intent);
    }

    @Override
    public void navigateToFinishConfigurationActivity(Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), FinishConfigurationActivity.class);
        fragment.startActivity(intent);
    }

    @Override
    public void showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }
}
