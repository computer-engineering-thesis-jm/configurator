package com.moreno.configurator.presentation.view.fragment;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.moreno.configurator.R;
import com.moreno.configurator.core.util.Provider;
import com.moreno.configurator.presentation.presenter.ChooseNetworkPresenter;
import com.moreno.configurator.presentation.view.model.NetworkView;
import com.moreno.configurator.presentation.view.receiver.WifiReceiver;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Action;
import io.reactivex.subjects.PublishSubject;


public class ChooseNetworkFragment extends Fragment {

    @BindView(R.id.network_list_view)
    ListView networkListView;

    private WifiManager wifiManager;
    private ChooseNetworkPresenter presenter;
    private ArrayAdapter<NetworkView> adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context applicationContext = getActivity().getApplicationContext();
        wifiManager = (WifiManager) applicationContext.getSystemService(Context.WIFI_SERVICE);
        presenter = Provider.getChooseNetworkPresenter(this, wifiManager);
        adapter = new NetworkListAdapter(getContext(), R.layout.network_item_view, new ArrayList<>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choose_network, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        networkListView.setAdapter(adapter);
        networkListView.setOnItemClickListener(presenter.onClickNetworkItemListener());
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void registerWifiReceiver(PublishSubject<String> ssidNetworksSubject) {
        getActivity().registerReceiver(
                new WifiReceiver(wifiManager, ssidNetworksSubject),
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    public void startScan() {
        wifiManager.startScan();
    }

    public void addNetworkView(String ssid) {
        NetworkView networkView = new NetworkView(getContext(), ssid);
        adapter.add(networkView);
    }

    private class NetworkListAdapter extends ArrayAdapter<NetworkView> {

        @NonNull
        private final List<NetworkView> networkViews;
        private final LayoutInflater inflater;
        private final int networkItemResource;

        public NetworkListAdapter(@NonNull Context context, int resourceId, @NonNull List<NetworkView> networkViews) {
            super(context, resourceId, networkViews);
            this.networkItemResource = resourceId;
            this.networkViews = networkViews;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return networkViews.size();
        }

        @Nullable
        @Override
        public NetworkView getItem(int position) {
            return networkViews.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = convertView;
            if (view == null) view = inflater.inflate(networkItemResource, null);
            TextView text = view.findViewById(R.id.ssid_label);
            text.setText(networkViews.get(position).getSsid());
            return view;
        }
    }
}
