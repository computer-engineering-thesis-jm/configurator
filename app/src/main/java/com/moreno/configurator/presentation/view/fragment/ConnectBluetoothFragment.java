package com.moreno.configurator.presentation.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import com.moreno.configurator.R;
import com.moreno.configurator.core.util.Provider;
import com.moreno.configurator.presentation.presenter.ConnectBluetoothPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ConnectBluetoothFragment extends Fragment {

    @BindView(R.id.connect_button)
    Button connectButton;

    @BindView(R.id.bluetooth_toggle_button)
    ToggleButton bluetoothToggleButton;
    private ConnectBluetoothPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = Provider.getConnectBluetoothPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_connect_bluetooth, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        connectButton.setOnClickListener(v -> presenter.connect());
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setBluetoothToggleState(Boolean state) {
        bluetoothToggleButton.setChecked(state);
    }
}
