package com.moreno.configurator.presentation.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.moreno.configurator.R;
import com.moreno.configurator.core.util.Provider;
import com.moreno.configurator.presentation.presenter.ConnectNetworkPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ConnectNetworkFragment extends Fragment {

    @BindView(R.id.connect_button)
    Button connectButton;

    @BindView(R.id.network_ssid)
    TextView networkSsid;

    @BindView(R.id.password_edit)
    EditText passwordEdit;

    private ConnectNetworkPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context applicationContext = getActivity().getApplicationContext();
        WifiManager wifiManager = (WifiManager) applicationContext.getSystemService(Context.WIFI_SERVICE);
        presenter = Provider.getConnectNetworkPresenter(this, wifiManager);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_connect_network, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        connectButton.setOnClickListener(v -> presenter.connect());
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setSsid(String aSsid) {
        networkSsid.setText(aSsid);
    }

    public Intent getIntent() {
        return getActivity().getIntent();
    }

    public String getSsid() {
        return String.valueOf(networkSsid.getText());
    }

    public String getPassword() {
        return String.valueOf(passwordEdit.getText());
    }
}
