package com.moreno.configurator.presentation.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.moreno.configurator.R;
import com.moreno.configurator.core.util.Provider;
import com.moreno.configurator.presentation.presenter.FinishConfigurationPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FinishConfigurationFragment extends Fragment {

    @BindView(R.id.device_id)
    TextView deviceId;

    @BindView(R.id.network_ssid)
    TextView networkSsid;

    @BindView(R.id.time)
    TextView time;

    @BindView(R.id.device_button)
    Button deviceButton;

    @BindView(R.id.wifi_button)
    Button wifiButton;

    @BindView(R.id.url_button)
    Button urlButton;

    @BindView(R.id.exit_button)
    Button exitButton;

    private FinishConfigurationPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = Provider.getFinishConfigurationPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_finish_configuration, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        presenter.onCreate();
    }

    @Override
    public void onResume() {
        super.onResume();
        deviceButton.setOnClickListener(t -> presenter.sendDeviceConfiguration());
        wifiButton.setOnClickListener(t -> presenter.sendWifiConfiguration());
        urlButton.setOnClickListener(t -> presenter.sendUrlServerConfiguration());
        exitButton.setOnClickListener(t -> presenter.exit());
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setDeviceId(String deviceId) {
        this.deviceId.setText(deviceId);
    }

    public void setNetworkSsid(String ssid) {
        this.networkSsid.setText(ssid);
    }

    public void setTime(String time) {
        this.time.setText(time);
    }

    public void enableDeviceButton() {
        deviceButton.setEnabled(true);
    }

    public void disableDeviceButton() {
        deviceButton.setEnabled(false);
    }

    public void enableWifiButton() {
        wifiButton.setEnabled(true);
    }

    public void disableWifiButton() {
        wifiButton.setEnabled(false);
    }

    public void enableUrlButton() {
        urlButton.setEnabled(true);
    }

    public void disableUrlButton() {
        urlButton.setEnabled(false);
    }

    public void enableExitButton() {
        exitButton.setVisibility(View.VISIBLE);
        exitButton.setEnabled(true);
    }

    public void disableExitButton() {
        exitButton.setVisibility(View.INVISIBLE);
        exitButton.setEnabled(false);
    }
}
