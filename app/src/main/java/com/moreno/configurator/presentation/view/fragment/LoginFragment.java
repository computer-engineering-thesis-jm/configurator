package com.moreno.configurator.presentation.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.moreno.configurator.R;
import com.moreno.configurator.core.util.Provider;
import com.moreno.configurator.presentation.presenter.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoginFragment extends Fragment {

    @BindView(R.id.sign_in_button)
    Button signInButton;

    @BindView(R.id.user_default)
    Button setUserDefaultCredentials;

    @BindView(R.id.device_id_edit)
    EditText deviceIdEditText;

    @BindView(R.id.pass_edit)
    EditText passwordEditText;

    private LoginPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = Provider.getLoginPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        signInButton.setOnClickListener(v -> presenter.signIn().subscribe());
        setUserDefaultCredentials.setOnClickListener(v -> {
            deviceIdEditText.setText("device1");
            passwordEditText.setText("sol1234");
        });
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    public void disableSignInButton() {
        signInButton.setEnabled(false);
    }

    public void enableSignInButton() {
        signInButton.setEnabled(true);
    }

    public void setTextChangedListenerOnDeviceIdEditText(TextWatcher textWatcher) {
        deviceIdEditText.addTextChangedListener(textWatcher);
    }

    public String getDeviceId() {
        return deviceIdEditText.getText().toString();
    }

    public String getPassword() {
        return passwordEditText.getText().toString();
    }
}
