package com.moreno.configurator.presentation.view.model;


import android.content.Context;
import android.view.View;

public class NetworkView extends View {

    private static final String EMPTY = "";
    private final String ssid;

    public NetworkView(Context context) {
        super(context);
        this.ssid = EMPTY;
    }

    public NetworkView(Context context, String ssid) {
        super(context);
        this.ssid = ssid;
    }

    public String getSsid() {
        return ssid;
    }
}
