package com.moreno.configurator.presentation.view.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class WifiReceiver extends BroadcastReceiver {

    private final WifiManager mainWifi;
    private final PublishSubject<String> ssidNetworksSubject;

    public WifiReceiver(WifiManager mainWifi, PublishSubject<String> ssidNetworksSubject) {
        this.mainWifi = mainWifi;
        this.ssidNetworksSubject = ssidNetworksSubject;
    }

    public void onReceive(Context c, Intent intent) {
        Observable.fromIterable(mainWifi.getScanResults())
                .subscribe(scanResult -> ssidNetworksSubject.onNext(scanResult.SSID));
    }
}
