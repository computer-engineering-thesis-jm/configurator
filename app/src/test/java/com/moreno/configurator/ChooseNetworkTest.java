package com.moreno.configurator;

import android.view.View;

import com.moreno.configurator.core.domain.action.network.SearchWifiNetworks;
import com.moreno.configurator.core.domain.action.network.VerifyWifiNetworkConnection;
import com.moreno.configurator.core.domain.model.NetworkService;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.presenter.ChooseNetworkPresenter;
import com.moreno.configurator.presentation.view.fragment.ChooseNetworkFragment;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChooseNetworkTest {

    private ChooseNetworkPresenter chooseNetworkPresenter;
    private NetworkService networkService;
    private ChooseNetworkFragment view;
    private PublishSubject<String> ssidNetworksSubject;

    @Before
    public void setUp() throws Exception {
        view = mock(ChooseNetworkFragment.class);
        networkService = mock(NetworkService.class);
        ssidNetworksSubject = PublishSubject.create();
        Navigator navigator = mock(Navigator.class);
        VerifyWifiNetworkConnection verifyWifiNetworkConnection = new VerifyWifiNetworkConnection(networkService);
        SearchWifiNetworks searchNetworks = new SearchWifiNetworks(ssidNetworksSubject);
        chooseNetworkPresenter = new ChooseNetworkPresenter(view,
                verifyWifiNetworkConnection, searchNetworks, ssidNetworksSubject, navigator);
    }

    @Test
    public void whenOnResumeViewAndWifiIsConnectedThenListAllAvailableNetworks() throws Exception {
        String ssid = "aSSID";
        when(networkService.isActive()).thenReturn(Single.just(Boolean.TRUE));
        chooseNetworkPresenter.onResume();

        ssidNetworksSubject.onNext(ssid);

        verify(view).addNetworkView(eq(ssid));
    }

    @Test
    public void whenOnResumeViewAndWifiIsNotConnectedThenDoNothing() throws Exception {
        String ssid = "aSSID";
        when(networkService.isActive()).thenReturn(Single.just(Boolean.FALSE));
        chooseNetworkPresenter.onResume();

        ssidNetworksSubject.onNext(ssid);

        verify(view, never()).addNetworkView(eq(ssid));
    }
}
