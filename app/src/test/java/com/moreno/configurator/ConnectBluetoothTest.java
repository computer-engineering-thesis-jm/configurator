package com.moreno.configurator;


import com.moreno.configurator.core.domain.action.bluetooth.ConnectBluetooth;
import com.moreno.configurator.core.domain.action.bluetooth.SwitchBluetoothState;
import com.moreno.configurator.core.domain.model.TransportLayerService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.UserInformation;
import com.moreno.configurator.core.infrastructure.UserInformationRepository;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.presenter.ConnectBluetoothPresenter;
import com.moreno.configurator.presentation.view.fragment.ConnectBluetoothFragment;

import org.junit.Test;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConnectBluetoothTest {

    @Test
    public void whenBluetoothToggleIsPressedAndBluetoothHasOffStateThenServiceActiveTheBluetooth() throws Exception {
        assertBluetoothToggleState(Single.just(true), true);
    }

    @Test
    public void whenBluetoothToggleIsPressedAndBluetoothHasOnStateThenServiceDeactivateTheBluetooth() throws Exception {
        assertBluetoothToggleState(Single.just(false), false);
    }

    private void assertBluetoothToggleState(Single<Boolean> just, boolean state) {
        Navigator navigator = mock(Navigator.class);
        ConnectBluetoothFragment connectBluetoothFragment = mock(ConnectBluetoothFragment.class);
        TransportLayerService transportLayerService = mock(TransportLayerService.class);
        when(transportLayerService.switchState()).thenReturn(just);
        UserRepository userRepository = new UserInformationRepository();
        SwitchBluetoothState switchBluetoothState = new SwitchBluetoothState(transportLayerService);
        ConnectBluetooth connectBluetooth = new ConnectBluetooth(transportLayerService, userRepository);
        ConnectBluetoothPresenter connectBluetoothPresenter = new ConnectBluetoothPresenter(connectBluetoothFragment, switchBluetoothState, connectBluetooth, navigator);

        connectBluetoothPresenter.onBluetoothToggleIsPressed();

        verify(connectBluetoothFragment).setBluetoothToggleState(state);
    }

    @Test
    public void whenOnResumeViewThenSynchronizeBluetoothState() throws Exception {
        Navigator navigator = mock(Navigator.class);
        ConnectBluetoothFragment connectBluetoothFragment = mock(ConnectBluetoothFragment.class);
        TransportLayerService transportLayerService = mock(TransportLayerService.class);
        when(transportLayerService.switchState()).thenReturn(Single.just(true));
        UserRepository userRepository = new UserInformationRepository();
        SwitchBluetoothState switchBluetoothState = new SwitchBluetoothState(transportLayerService);
        ConnectBluetooth connectBluetooth = new ConnectBluetooth(transportLayerService, userRepository);
        ConnectBluetoothPresenter connectBluetoothPresenter = new ConnectBluetoothPresenter(connectBluetoothFragment, switchBluetoothState, connectBluetooth, navigator);

        connectBluetoothPresenter.onResume();

        verify(connectBluetoothFragment).setBluetoothToggleState(true);
    }

    @Test
    public void whenConnectButtonIsPressedAndHasAValidIdentifierOnRepositoryThenNavigateToFinishConfigurationActivity() throws Exception {
        String identifier = "00001101-0000-1000-8000-00805f9b34fb";
        Navigator navigator = mock(Navigator.class);
        ConnectBluetoothFragment view = mock(ConnectBluetoothFragment.class);
        TransportLayerService transportLayerService = mock(TransportLayerService.class);
        when(transportLayerService.connect(identifier)).thenReturn(Single.just(true));
        UserRepository userRepository = new UserInformationRepository();
        SwitchBluetoothState switchBluetoothState = new SwitchBluetoothState(transportLayerService);
        ConnectBluetooth connectBluetooth = new ConnectBluetooth(transportLayerService, userRepository);
        ConnectBluetoothPresenter connectBluetoothPresenter = new ConnectBluetoothPresenter(view, switchBluetoothState, connectBluetooth, navigator);

        userRepository.putUserInformation(new UserInformation("aDeviceId", "aPassword", identifier));

        connectBluetoothPresenter.connect();

        verify(navigator).navigateToFinishConfigurationActivity(view);
    }

    @Test
    public void whenConnectButtonIsPressedAndHasAnInvalidIdentifierOnRepositoryThenNotNavigate() throws Exception {
        Navigator navigator = mock(Navigator.class);
        ConnectBluetoothFragment view = mock(ConnectBluetoothFragment.class);
        TransportLayerService transportLayerService = mock(TransportLayerService.class);
        when(transportLayerService.connect(anyString())).thenReturn(Single.just(false));
        UserRepository userRepository = new UserInformationRepository();
        SwitchBluetoothState switchBluetoothState = new SwitchBluetoothState(transportLayerService);
        ConnectBluetooth connectBluetooth = new ConnectBluetooth(transportLayerService, userRepository);
        ConnectBluetoothPresenter connectBluetoothPresenter = new ConnectBluetoothPresenter(view, switchBluetoothState, connectBluetooth, navigator);

        userRepository.putUserInformation(new UserInformation("aDeviceId", "aPassword", "anyIdentifier"));

        connectBluetoothPresenter.connect();

        verify(navigator, never()).navigateToFinishConfigurationActivity(view);
    }
}
