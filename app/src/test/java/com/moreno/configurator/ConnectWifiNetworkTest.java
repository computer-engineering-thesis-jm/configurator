package com.moreno.configurator;


import android.content.Intent;

import com.moreno.configurator.core.domain.action.network.ConnectWifiNetwork;
import com.moreno.configurator.core.domain.model.NetworkService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.infrastructure.UserInformationRepository;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.presenter.ConnectNetworkPresenter;
import com.moreno.configurator.presentation.view.fragment.ConnectNetworkFragment;

import org.junit.Test;

import io.reactivex.Single;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConnectWifiNetworkTest {

    @Test
    public void whenOnResumeThenShowSsidOnLabel() throws Exception {
        String ssid = "aSsid";
        ConnectNetworkFragment connectNetworkFragment = mock(ConnectNetworkFragment.class);
        Intent intent = mock(Intent.class);
        when(intent.getStringExtra("NETWORK_SSID")).thenReturn(ssid);
        when(connectNetworkFragment.getIntent()).thenReturn(intent);
        Navigator navigator = mock(Navigator.class);
        NetworkService networkService = mock(NetworkService.class);
        UserRepository userRepository = new UserInformationRepository();
        ConnectWifiNetwork connectWifiNetwork = new ConnectWifiNetwork(networkService, userRepository);
        ConnectNetworkPresenter presenter = new ConnectNetworkPresenter(connectNetworkFragment,
                connectWifiNetwork, navigator);

        presenter.onResume();

        verify(connectNetworkFragment).setSsid(ssid);
    }

    @Test
    public void whenClickOnConnectButtonAndIsValidNetworkThenNavigateToConnectBluetoothActivity() throws Exception {
        String ssid = "aSsid";
        String password = "aPassword";
        ConnectNetworkFragment view = mock(ConnectNetworkFragment.class);
        when(view.getSsid()).thenReturn(ssid);
        when(view.getPassword()).thenReturn(password);
        Navigator navigator = mock(Navigator.class);
        NetworkService networkService = mock(NetworkService.class);
        when(networkService.isValidNetwork(ssid, password)).thenReturn(Single.just(true));
        UserRepository userRepository = new UserInformationRepository();
        ConnectWifiNetwork connectWifiNetwork = new ConnectWifiNetwork(networkService, userRepository);
        ConnectNetworkPresenter presenter = new ConnectNetworkPresenter(view,
                connectWifiNetwork, navigator);

        presenter.connect();

        verify(navigator).navigateToConnectBluetoothActivity(view);
    }
}
