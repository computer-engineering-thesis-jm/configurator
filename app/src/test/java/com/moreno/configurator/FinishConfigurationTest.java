package com.moreno.configurator;

import com.moreno.configurator.core.domain.action.configure.ListenCompletedAck;
import com.moreno.configurator.core.domain.action.configure.ListenDeviceConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.ListenUrlServerConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.ListenWifiConfiguredAck;
import com.moreno.configurator.core.domain.action.configure.SendDeviceInformation;
import com.moreno.configurator.core.domain.action.configure.SendUrlServerInformation;
import com.moreno.configurator.core.domain.action.configure.SendWifiInformation;
import com.moreno.configurator.core.domain.model.TimeService;
import com.moreno.configurator.core.domain.model.TransportLayerService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.NetworkInformation;
import com.moreno.configurator.core.domain.model.network.UserInformation;
import com.moreno.configurator.core.infrastructure.UserInformationRepository;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.action.FillFieldsWithInformation;
import com.moreno.configurator.presentation.presenter.FinishConfigurationPresenter;
import com.moreno.configurator.presentation.view.fragment.FinishConfigurationFragment;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import io.reactivex.subjects.PublishSubject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FinishConfigurationTest {

    private static final String API_ENDPOINT = "api.endpoint";
    private static final Integer API_PORT = 8080;
    private static final String DEVICE_ID = "aDeviceId";
    private static final String SSID = "aNetworkId";
    private static final Long TIME_AS_LONG = 123L;

    private FinishConfigurationPresenter presenter;
    private FinishConfigurationFragment view;
    private TimeService timeService;
    private UserRepository repository;
    private TransportLayerService transportLayerService;
    private PublishSubject<String> bluetoothMessageSubject;
    private Navigator navigator;

    @Before
    public void setUp() throws Exception {
        view = mock(FinishConfigurationFragment.class);
        FillFieldsWithInformation fillFieldsWithInformation = new FillFieldsWithInformation(view, timeService, repository);
        timeService = mock(TimeService.class);
        repository = new UserInformationRepository();
        repository.putUserInformation(new UserInformation(DEVICE_ID, "123", "123"));
        repository.putNetwork(new NetworkInformation(SSID, "123"));
        transportLayerService = mock(TransportLayerService.class);
        bluetoothMessageSubject = PublishSubject.create();
        navigator = mock(Navigator.class);
        SendDeviceInformation sendDeviceInformation = new SendDeviceInformation(transportLayerService, repository);
        SendUrlServerInformation sendUrlServerInformation = new SendUrlServerInformation(transportLayerService, API_ENDPOINT, API_PORT);
        SendWifiInformation sendWifiInformation = new SendWifiInformation(transportLayerService, repository);
        ListenCompletedAck listenCompletedAck = new ListenCompletedAck(bluetoothMessageSubject);
        ListenDeviceConfiguredAck listenDeviceConfiguredAck = new ListenDeviceConfiguredAck(bluetoothMessageSubject);
        ListenWifiConfiguredAck listenWifiConfiguredAck = new ListenWifiConfiguredAck(bluetoothMessageSubject);
        ListenUrlServerConfiguredAck listenUrlServerConfiguredAck = new ListenUrlServerConfiguredAck(bluetoothMessageSubject);
        presenter = new FinishConfigurationPresenter(view, fillFieldsWithInformation,
                sendDeviceInformation, sendWifiInformation, sendUrlServerInformation,
                listenCompletedAck, listenDeviceConfiguredAck, listenWifiConfiguredAck, listenUrlServerConfiguredAck,
                navigator);
    }

    @Test
    public void whenOnResumeThenSetDeviceIdOnTextView() throws Exception {
        presenter.onResume();

        verify(view).setDeviceId(DEVICE_ID);
    }

    @Test
    public void whenOnResumeThenSetNetworkOnTextView() throws Exception {
        presenter.onResume();

        verify(view).setNetworkSsid(SSID);
    }

    @Test
    public void whenOnResumeThenSetTimeOnTextView() throws Exception {
        when(timeService.getTime()).thenReturn(TIME_AS_LONG);
        presenter.onResume();

        verify(view).setTime(new Date(TIME_AS_LONG).toString());
    }

    @Test
    public void whenSendButtonIsPressedThenSendInformationByBluetooth() throws Exception {
        when(timeService.getTime()).thenReturn(TIME_AS_LONG);
        UserInformation userInformation = new UserInformation("aDeviceId", "123", "123");
        NetworkInformation networkInformation = new NetworkInformation("aSsid", "123");
        String information = createInformation(userInformation, networkInformation);

        presenter.sendDeviceConfiguration();

        verify(transportLayerService).send(information);
    }

    @Test
    public void whenArrivesAckThenShowToast() throws Exception {

    }

    private String createInformation(UserInformation userInformation, NetworkInformation networkInformation) throws JSONException {
        return new JSONObject()
                .put("did", userInformation.getDeviceId())
                .put("tkn", userInformation.getUuidToken())
                .put("sid", networkInformation.getSsid())
                .put("pss", networkInformation.getPassword())
                .put("url", API_ENDPOINT)
                .put("prt", API_PORT)
                .toString();
    }
}
