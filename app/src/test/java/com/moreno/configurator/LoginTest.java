package com.moreno.configurator;

import com.moreno.configurator.core.domain.action.login.LoginAction;
import com.moreno.configurator.core.domain.model.ApiService;
import com.moreno.configurator.core.domain.model.UserRepository;
import com.moreno.configurator.core.domain.model.network.LoginResult;
import com.moreno.configurator.core.domain.model.network.UserInformation;
import com.moreno.configurator.core.infrastructure.UserInformationRepository;
import com.moreno.configurator.core.infrastructure.UserInformationService;
import com.moreno.configurator.presentation.Navigator;
import com.moreno.configurator.presentation.presenter.LoginPresenter;
import com.moreno.configurator.presentation.view.fragment.LoginFragment;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import okhttp3.OkHttpClient;
import okhttp3.mock.MockInterceptor;
import okhttp3.mock.Rule;

import static okhttp3.mock.MediaTypes.MEDIATYPE_JSON;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class LoginTest {

    private static final String ENDPOINT = "http://localhost";
    private static final Integer API_PORT = 8080;
    private static final String ANONYMOUS_TOKEN_VALUE = "anonymous_token";
    private static final String RESPONSE_TOKEN_VALUE = "anUUID";
    private static final String URL = ENDPOINT + ":" + API_PORT + "/user/login";

    private LoginFragment view;
    private Navigator navigator;
    private UserRepository repository;

    @Before
    public void setUp() throws Exception {
        view = mock(LoginFragment.class);
        repository = new UserInformationRepository();
        navigator = mock(Navigator.class);
    }

    @Test
    public void whenCreateLoginViewThenDisableSignInButton() throws Exception {
        ApiService apiService = mock(ApiService.class);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);
        loginPresenter.onResume();

        verify(view).disableSignInButton();
    }

    @Test
    @Ignore
    public void whenInsertAnyTextOnDeviceIdEditTextThenEnableSignInButton() throws Exception {
        ApiService apiService = mock(ApiService.class);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        loginPresenter.onResume();

        verify(view).enableSignInButton();
    }

    @Test
    public void whenSignInButtonIsPressedThenDoLogin() throws Exception {
        String deviceId = "aDeviceId";
        String password = "aPassword";

        ApiService apiService = mock(ApiService.class);
        when(apiService.login(deviceId, password)).thenReturn(Single.just(new LoginResult(deviceId, password, true, ANONYMOUS_TOKEN_VALUE)));
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        when(view.getDeviceId()).thenReturn(deviceId);
        when(view.getPassword()).thenReturn(password);

        loginPresenter.signIn().test().await();

        verify(apiService).login(deviceId, password);
    }

    @Test
    public void whenTryToSignInAndDeviceIdIsEmptyThenDoesNotLogin() throws Exception {
        String deviceId = "";
        String password = "aPassword";

        ApiService apiService = mock(ApiService.class);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        when(view.getDeviceId()).thenReturn(deviceId);
        when(view.getPassword()).thenReturn(password);

        loginPresenter.signIn();

        verify(apiService, never()).login(deviceId, password);
    }

    @Test
    public void whenTryToSignInAndPasswordIsEmptyThenDoesNotLogin() throws Exception {
        String deviceId = "aDeviceId";
        String password = "";

        ApiService apiService = mock(ApiService.class);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        when(view.getDeviceId()).thenReturn(deviceId);
        when(view.getPassword()).thenReturn(password);

        loginPresenter.signIn();

        verify(apiService, never()).login(deviceId, password);
    }

    @Test
    public void whenLoginIsSuccessThenNavigateToChooseNetworkActivity() throws Exception {
        String deviceId = "aDeviceId";
        String password = "aPassword";

        OkHttpClient okHttpClient = createOkHttpClient(getSuccessBodyWithContent(RESPONSE_TOKEN_VALUE));
        UserInformationService apiService = new UserInformationService(okHttpClient, ENDPOINT, API_PORT, ANONYMOUS_TOKEN_VALUE);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        when(view.getDeviceId()).thenReturn(deviceId);
        when(view.getPassword()).thenReturn(password);

        loginPresenter.signIn().test().await();

        verify(navigator).navigateToChooseNetworkActivity(view);
    }

    @Test
    public void whenLoginIsFailureThenNotNavigate() throws Exception {
        String deviceId = "aDeviceId";
        String password = "aPassword";

        OkHttpClient okHttpClient = createOkHttpClient(getFailureBody());
        UserInformationService apiService = new UserInformationService(okHttpClient, ENDPOINT, API_PORT, ANONYMOUS_TOKEN_VALUE);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        when(view.getDeviceId()).thenReturn(deviceId);
        when(view.getPassword()).thenReturn(password);

        loginPresenter.signIn().test().await();

        verify(navigator, never()).navigateToChooseNetworkActivity(view);
    }

    @Test
    public void whenLoginSuccessThenPutUserInformationOnRepository() throws Exception {
        String deviceId = "aDeviceId";
        String password = "aPassword";
        String uuid = "anUUID";

        OkHttpClient okHttpClient = createOkHttpClient(getSuccessBodyWithContent(RESPONSE_TOKEN_VALUE));
        UserInformationService apiService = new UserInformationService(okHttpClient, ENDPOINT, API_PORT, ANONYMOUS_TOKEN_VALUE);
        LoginAction loginAction = new LoginAction(apiService, repository);
        LoginPresenter loginPresenter = new LoginPresenter(view, loginAction, navigator);

        when(view.getDeviceId()).thenReturn(deviceId);
        when(view.getPassword()).thenReturn(password);

        loginPresenter.signIn().test().await();

        UserInformation userInformation = new UserInformation(deviceId, password, uuid);
        Assert.assertNotNull(repository.getUserInformation());
        Assert.assertEquals(userInformation, repository.getUserInformation());
    }

    private OkHttpClient createOkHttpClient(String successBodyWithContent) {
        MockInterceptor interceptor = new MockInterceptor();
        interceptor.addRule(new Rule.Builder()
                .post()
                .url(URL)
                .respond(successBodyWithContent, MEDIATYPE_JSON)
                .header("Authorization", ANONYMOUS_TOKEN_VALUE));
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    private String getSuccessBodyWithContent(String token) {
        return "{\"success\":true, \"content\":{ \"token\":\"" + token + "\" }}";
    }

    private String getFailureBody() {
        return "{\"success\":false, \"content\":{ }}";
    }
}